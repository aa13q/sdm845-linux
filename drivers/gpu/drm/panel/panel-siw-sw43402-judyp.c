// SPDX-License-Identifier: GPL-2.0-only
// Copyright (c) 2021 FIXME
// Generated with linux-mdss-dsi-panel-driver-generator from vendor device tree:
//   Copyright (c) 2013, The Linux Foundation. All rights reserved. (FIXME)

#include <linux/backlight.h>
#include <linux/delay.h>
#include <linux/gpio/consumer.h>
#include <linux/module.h>
#include <linux/of.h>

#include <drm/drm_mipi_dsi.h>
#include <drm/drm_modes.h>
#include <drm/drm_panel.h>

struct sw43402_dsc {
	struct drm_panel panel;
	struct mipi_dsi_device *dsi;
	struct gpio_desc *reset_gpio;
	bool prepared;
};

static inline struct sw43402_dsc *to_sw43402_dsc(struct drm_panel *panel)
{
	return container_of(panel, struct sw43402_dsc, panel);
}

#define dsi_dcs_write_seq(dsi, seq...) do {				\
		static const u8 d[] = { seq };				\
		int ret;						\
		ret = mipi_dsi_dcs_write_buffer(dsi, d, ARRAY_SIZE(d));	\
		if (ret < 0)						\
			return ret;					\
	} while (0)

static void sw43402_dsc_reset(struct sw43402_dsc *ctx)
{
	gpiod_set_value_cansleep(ctx->reset_gpio, 0);
	usleep_range(10000, 11000);
	gpiod_set_value_cansleep(ctx->reset_gpio, 1);
	usleep_range(10000, 11000);
	gpiod_set_value_cansleep(ctx->reset_gpio, 0);
	msleep(25);
}

static int sw43402_dsc_on(struct sw43402_dsc *ctx)
{
	struct mipi_dsi_device *dsi = ctx->dsi;
	struct device *dev = &dsi->dev;
	int ret;

	dsi_dcs_write_seq(dsi, 0xb0, 0x20, 0x43);
	dsi_dcs_write_seq(dsi, 0x3d, 0x00);
	dsi_dcs_write_seq(dsi, 0xf2, 0x00);
	dsi_dcs_write_seq(dsi, 0xff, 0x03, 0x00);
	usleep_range(1000, 2000);
	dsi_dcs_write_seq(dsi, 0x35);

	ret = mipi_dsi_dcs_exit_sleep_mode(dsi);
	if (ret < 0) {
		dev_err(dev, "Failed to exit sleep mode: %d\n", ret);
		return ret;
	}
	msleep(60);

	dsi_dcs_write_seq(dsi, 0x51, 0x03);
	dsi_dcs_write_seq(dsi, 0x53, 0x07);
	dsi_dcs_write_seq(dsi, 0x55, 0x0c);
	dsi_dcs_write_seq(dsi, 0xb0, 0xa5, 0x00);
	dsi_dcs_write_seq(dsi, 0xb2,
			  0x5d, 0x41, 0x04, 0x8c, 0x00, 0xff, 0xff, 0x15, 0x00,
			  0x00, 0x00, 0x00);
	dsi_dcs_write_seq(dsi, 0xe8, 0x08, 0x90, 0x10, 0x25);
	dsi_dcs_write_seq(dsi, 0xd4,
			  0x10, 0x00, 0xff, 0x60, 0x30, 0x40, 0x50, 0x20, 0x20,
			  0x20, 0x20, 0xa0, 0x00, 0x20, 0x00, 0x34, 0xa0, 0x08,
			  0xda, 0xda, 0x4a);
	dsi_dcs_write_seq(dsi, 0xfb, 0x03, 0x77);
	dsi_dcs_write_seq(dsi, 0xed, 0x13, 0x00, 0x07, 0x00, 0x13);
	dsi_dcs_write_seq(dsi, 0xe2,
			  0x20, 0x0d, 0x08, 0xa8, 0x0a, 0xaa, 0x04, 0xa4, 0x80,
			  0x80, 0x80, 0x5c, 0x5c, 0x5c);
	dsi_dcs_write_seq(dsi, 0xe7,
			  0x00, 0x0d, 0x76, 0x1f, 0x00, 0x0d, 0x4a, 0x44, 0x0d,
			  0x76, 0x25, 0x00, 0x0d, 0x0d, 0x0d, 0x0d, 0x4a, 0x00);
	dsi_dcs_write_seq(dsi, 0xce,
			  0x81, 0x1f, 0x0f, 0x01, 0x24, 0x68, 0x22, 0x20, 0x04,
			  0x01, 0x00, 0x80, 0xff, 0x88, 0x08, 0x02, 0x00, 0x00);
	msleep(90);
	dsi_dcs_write_seq(dsi, 0xe7,
			  0x00, 0x0d, 0x76, 0x1f, 0x00, 0x0d, 0x0d, 0x44, 0x0d,
			  0x76, 0x25, 0x00, 0x0d, 0x0d, 0x0d, 0x0d, 0x4a, 0x00);
	msleep(70);

	return 0;
}

static int sw43402_dsc_off(struct sw43402_dsc *ctx)
{
	struct mipi_dsi_device *dsi = ctx->dsi;
	struct device *dev = &dsi->dev;
	int ret;

	dsi_dcs_write_seq(dsi, 0xb0, 0xa5, 0x00);
	msleep(60);
	dsi_dcs_write_seq(dsi, 0xca, 0x00, 0x06, 0x00, 0x06, 0x00, 0x16, 0x10);
	dsi_dcs_write_seq(dsi, 0xcb,
			  0x0b, 0x68, 0x00, 0x0b, 0x68, 0x00, 0x0b, 0x68, 0x00,
			  0x0b, 0x68, 0x00, 0x0b, 0x68, 0x00, 0x0b, 0x68, 0x00,
			  0x0b, 0x68, 0x00, 0x0b, 0x68, 0x00, 0x0b, 0x68, 0x00,
			  0x0b, 0x68, 0x00);
	dsi_dcs_write_seq(dsi, 0xcc,
			  0x0b, 0x68, 0x00, 0x0b, 0x68, 0x00, 0x0b, 0x68, 0x00,
			  0x05, 0xb4, 0x00, 0x05, 0xb4, 0x00, 0x55, 0x12, 0x13);
	dsi_dcs_write_seq(dsi, 0xe8, 0x08, 0x90, 0x10, 0x25);

	ret = mipi_dsi_dcs_set_display_off(dsi);
	if (ret < 0) {
		dev_err(dev, "Failed to set display off: %d\n", ret);
		return ret;
	}

	ret = mipi_dsi_dcs_enter_sleep_mode(dsi);
	if (ret < 0) {
		dev_err(dev, "Failed to enter sleep mode: %d\n", ret);
		return ret;
	}
	msleep(150);

	dsi_dcs_write_seq(dsi, 0xb0, 0x00, 0x00);

	return 0;
}

static int sw43402_dsc_prepare(struct drm_panel *panel)
{
	struct sw43402_dsc *ctx = to_sw43402_dsc(panel);
	struct device *dev = &ctx->dsi->dev;
	int ret;

	if (ctx->prepared)
		return 0;

	sw43402_dsc_reset(ctx);

	ret = sw43402_dsc_on(ctx);
	if (ret < 0) {
		dev_err(dev, "Failed to initialize panel: %d\n", ret);
		gpiod_set_value_cansleep(ctx->reset_gpio, 1);
		return ret;
	}

	ctx->prepared = true;
	return 0;
}

static int sw43402_dsc_unprepare(struct drm_panel *panel)
{
	struct sw43402_dsc *ctx = to_sw43402_dsc(panel);
	struct device *dev = &ctx->dsi->dev;
	int ret;

	if (!ctx->prepared)
		return 0;

	ret = sw43402_dsc_off(ctx);
	if (ret < 0)
		dev_err(dev, "Failed to un-initialize panel: %d\n", ret);

	gpiod_set_value_cansleep(ctx->reset_gpio, 1);

	ctx->prepared = false;
	return 0;
}

static const struct drm_display_mode sw43402_dsc_mode = {
	.clock = (1440 + 92 + 32 + 48) * (2880 + 10 + 1 + 25) * 60 / 1000,
	.hdisplay = 1440,
	.hsync_start = 1440 + 92,
	.hsync_end = 1440 + 92 + 32,
	.htotal = 1440 + 92 + 32 + 48,
	.vdisplay = 2880,
	.vsync_start = 2880 + 10,
	.vsync_end = 2880 + 10 + 1,
	.vtotal = 2880 + 10 + 1 + 25,
	.width_mm = 68,
	.height_mm = 136,
};

static int sw43402_dsc_get_modes(struct drm_panel *panel,
				 struct drm_connector *connector)
{
	struct drm_display_mode *mode;

	mode = drm_mode_duplicate(connector->dev, &sw43402_dsc_mode);
	if (!mode)
		return -ENOMEM;

	drm_mode_set_name(mode);

	mode->type = DRM_MODE_TYPE_DRIVER | DRM_MODE_TYPE_PREFERRED;
	connector->display_info.width_mm = mode->width_mm;
	connector->display_info.height_mm = mode->height_mm;
	drm_mode_probed_add(connector, mode);

	return 1;
}

static const struct drm_panel_funcs sw43402_dsc_panel_funcs = {
	.prepare = sw43402_dsc_prepare,
	.unprepare = sw43402_dsc_unprepare,
	.get_modes = sw43402_dsc_get_modes,
};

static int sw43402_dsc_bl_update_status(struct backlight_device *bl)
{
	struct mipi_dsi_device *dsi = bl_get_data(bl);
	u16 brightness = backlight_get_brightness(bl);
	int ret;

	dsi->mode_flags &= ~MIPI_DSI_MODE_LPM;

	ret = mipi_dsi_dcs_set_display_brightness(dsi, brightness);
	if (ret < 0)
		return ret;

	dsi->mode_flags |= MIPI_DSI_MODE_LPM;

	return 0;
}

// TODO: Check if /sys/class/backlight/.../actual_brightness actually returns
// correct values. If not, remove this function.
static int sw43402_dsc_bl_get_brightness(struct backlight_device *bl)
{
	struct mipi_dsi_device *dsi = bl_get_data(bl);
	u16 brightness;
	int ret;

	dsi->mode_flags &= ~MIPI_DSI_MODE_LPM;

	ret = mipi_dsi_dcs_get_display_brightness(dsi, &brightness);
	if (ret < 0)
		return ret;

	dsi->mode_flags |= MIPI_DSI_MODE_LPM;

	return brightness;
}

static const struct backlight_ops sw43402_dsc_bl_ops = {
	.update_status = sw43402_dsc_bl_update_status,
	.get_brightness = sw43402_dsc_bl_get_brightness,
};

static struct backlight_device *
sw43402_dsc_create_backlight(struct mipi_dsi_device *dsi)
{
	struct device *dev = &dsi->dev;
	const struct backlight_properties props = {
		.type = BACKLIGHT_RAW,
		.brightness = 511,
		.max_brightness = 511,
	};

	return devm_backlight_device_register(dev, dev_name(dev), dev, dsi,
					      &sw43402_dsc_bl_ops, &props);
}

static int sw43402_dsc_probe(struct mipi_dsi_device *dsi)
{
	struct device *dev = &dsi->dev;
	struct sw43402_dsc *ctx;
	int ret;

	ctx = devm_kzalloc(dev, sizeof(*ctx), GFP_KERNEL);
	if (!ctx)
		return -ENOMEM;

	ctx->reset_gpio = devm_gpiod_get(dev, "reset", GPIOD_OUT_HIGH);
	if (IS_ERR(ctx->reset_gpio))
		return dev_err_probe(dev, PTR_ERR(ctx->reset_gpio),
				     "Failed to get reset-gpios\n");

	ctx->dsi = dsi;
	mipi_dsi_set_drvdata(dsi, ctx);

	dsi->lanes = 4;
	dsi->format = MIPI_DSI_FMT_RGB888;
	dsi->mode_flags = MIPI_DSI_MODE_VIDEO_BURST |
			  MIPI_DSI_CLOCK_NON_CONTINUOUS | MIPI_DSI_MODE_LPM;

	drm_panel_init(&ctx->panel, dev, &sw43402_dsc_panel_funcs,
		       DRM_MODE_CONNECTOR_DSI);

	ctx->panel.backlight = sw43402_dsc_create_backlight(dsi);
	if (IS_ERR(ctx->panel.backlight))
		return dev_err_probe(dev, PTR_ERR(ctx->panel.backlight),
				     "Failed to create backlight\n");

	drm_panel_add(&ctx->panel);

	ret = mipi_dsi_attach(dsi);
	if (ret < 0) {
		dev_err(dev, "Failed to attach to DSI host: %d\n", ret);
		drm_panel_remove(&ctx->panel);
		return ret;
	}

	return 0;
}

static int sw43402_dsc_remove(struct mipi_dsi_device *dsi)
{
	struct sw43402_dsc *ctx = mipi_dsi_get_drvdata(dsi);
	int ret;

	ret = mipi_dsi_detach(dsi);
	if (ret < 0)
		dev_err(&dsi->dev, "Failed to detach from DSI host: %d\n", ret);

	drm_panel_remove(&ctx->panel);

	return 0;
}

static const struct of_device_id sw43402_dsc_of_match[] = {
	{ .compatible = "siw,sw43402-judyp" },
	{ /* sentinel */ }
};
MODULE_DEVICE_TABLE(of, sw43402_dsc_of_match);

static struct mipi_dsi_driver sw43402_dsc_driver = {
	.probe = sw43402_dsc_probe,
	.remove = sw43402_dsc_remove,
	.driver = {
		.name = "panel-sw43402-dsc",
		.of_match_table = sw43402_dsc_of_match,
	},
};
module_mipi_dsi_driver(sw43402_dsc_driver);

MODULE_AUTHOR("linux-mdss-dsi-panel-driver-generator <fix@me>"); // FIXME
MODULE_DESCRIPTION("DRM driver for SW43402 cmd mode dsc dsi panel");
MODULE_LICENSE("GPL v2");
